// URL_path.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef URL_path_h
#define URL_path_h

#include <iostream>

class URL_path
{	URL_path(const URL_path&) = delete;
	void operator=(const URL_path&) = delete;

public:
	~URL_path()
	{}
	URL_path()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const URL_path& uRL_path)
{	return uRL_path.Print(os);
}


inline
std::istream& operator>>(std::istream& is,URL_path& uRL_path)
{	return uRL_path.Input(is);
}

#endif
