// StringSplitter.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef StringSplitter_h
#define StringSplitter_h

#include <iostream>

class StringSplitter
{	StringSplitter(const StringSplitter&) = delete;
	void operator=(const StringSplitter&) = delete;

public:
	~StringSplitter()
	{}
	StringSplitter()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const StringSplitter& stringSplitter)
{	return stringSplitter.Print(os);
}


inline
std::istream& operator>>(std::istream& is,StringSplitter& stringSplitter)
{	return stringSplitter.Input(is);
}

#endif
