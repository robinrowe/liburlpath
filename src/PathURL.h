// PathURL.h 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#ifndef PathURL_h
#define PathURL_h

#include <iostream>

class PathURL
{	PathURL(const PathURL&) = delete;
	void operator=(const PathURL&) = delete;

public:
	~PathURL()
	{}
	PathURL()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const PathURL& pathURL)
{	return pathURL.Print(os);
}


inline
std::istream& operator>>(std::istream& is,PathURL& pathURL)
{	return pathURL.Input(is);
}

#endif
