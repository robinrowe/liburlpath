/*-------------------------------------------------------------------------
 * https://github.com/okbob/url_encode/blob/master/src/url_encode.c
 * url_encode
 *	  PostgreSQL functions for url code support
 *
 * Author:	Pavel Stehule
 * Postcardware licence @2012-2017
 *
 * IDENTIFICATION
 *	  url_encode/url_encode.c
 *
 *-------------------------------------------------------------------------
 */

#if 0
#include "postgres.h"

#include "fmgr.h"
#include "mb/pg_wchar.h"
#include "utils/builtins.h"


PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(url_encode);
PG_FUNCTION_INFO_V1(url_decode);
PG_FUNCTION_INFO_V1(uri_encode);
PG_FUNCTION_INFO_V1(uri_decode);

Datum url_encode(PG_FUNCTION_ARGS);
Datum url_decode(PG_FUNCTION_ARGS);
Datum uri_encode(PG_FUNCTION_ARGS);
Datum uri_decode(PG_FUNCTION_ARGS);
#else
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "url_encode.h"

typedef int8_t int8;
typedef text Datum;
typedef int32_t pg_wchar;

#define SET_VARSIZE(t,size) t[size] = 0
#define VARSIZE(in_text) (int) in_text->size()
#define VARDATA(in_text) (char*) in_text->c_str()
//Not sizeof(int32_t):
#define VARHDRSZ 0
#define elog(code,msg) puts(msg)
//#define palloc malloc
#define Assert assert
#define PG_RETURN_TEXT_P return
#define pg_mblen (int) strlen
#define pg_utf_mblen(s) (int) strlen((const char*)s)
#define PG_GETARG_TEXT_P
#define PG_FUNCTION_ARGS

unsigned char* unicode_to_utf8 	(pg_wchar c,unsigned char *utf8string) 		
 {
     if (c <= 0x7F)
     {
         utf8string[0] = c;
     }
     else if (c <= 0x7FF)
     {
         utf8string[0] = 0xC0 | ((c >> 6) & 0x1F);
         utf8string[1] = 0x80 | (c & 0x3F);
     }
     else if (c <= 0xFFFF)
     {
         utf8string[0] = 0xE0 | ((c >> 12) & 0x0F);
         utf8string[1] = 0x80 | ((c >> 6) & 0x3F);
         utf8string[2] = 0x80 | (c & 0x3F);
     }
     else
     {
         utf8string[0] = 0xF0 | ((c >> 18) & 0x07);
         utf8string[1] = 0x80 | ((c >> 12) & 0x3F);
         utf8string[2] = 0x80 | ((c >> 6) & 0x3F);
         utf8string[3] = 0x80 | (c & 0x3F);
     }
  
     return utf8string;
 }

#endif

static const char *hex_chars = "0123456789ABCDEF";

static const int8 hexlookup[128] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1,
	-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

static inline unsigned char
get_hex(char c)
{
	int			res = -1;

	if (c > 0 && c < 127)
		res = hexlookup[(unsigned char) c];

	if (res < 0)
#if 0
		ereport(ERROR,
				(errcode(ERRCODE_INVALID_PARAMETER_VALUE),
				 errmsg("invalid hexadecimal digit: \"%c\"", c)));
#else
		printf("invalid hexadecimal digit: \"%c\"", c);
#endif
	return (char) res;
}

text encode(text *in_text, const char *unreserved_special)
{
	int	   len;
	text		result;
	char		*read_ptr;
	char		*write_ptr;
	int		real_len;
	int	processed;
	int				i;

	len = VARSIZE(in_text) - VARHDRSZ;
	read_ptr = VARDATA(in_text);

	/* preallocation max 3 times of size */

	result.resize(3 * len);
	write_ptr = (char*) result.c_str();
	processed = 0;
	real_len = 0;
	while (processed < len)
	{
		int mblen = pg_mblen(read_ptr);

		if (mblen == 1)
		{
			char	c = *read_ptr;

			if ((c >='0' && c <= '9') ||
			    (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||
			    (strchr(unreserved_special, c) != NULL))
			{
				*write_ptr++ = c;
				real_len += 1;
				processed += 1;
				read_ptr += 1;

				continue;
			}
		}

		for (i = 0; i < mblen; i++)
		{
			unsigned char b = ((unsigned char *) read_ptr)[i];
			if(isalpha(b))
			{	*write_ptr++ = b;
				real_len++;
			}
			else
			{	*write_ptr++ = '%';
				*write_ptr++ = hex_chars[(b >> 4) & 0xF];
				*write_ptr++ = hex_chars[b & 0xF];
				real_len += 3;
		}	}

		processed += mblen;
		read_ptr += mblen;
	}
//	write_ptr[real_len] = 0;
//	SET_VARSIZE(result, real_len + VARHDRSZ);

	return result;
}

static uint32_t
decode_utf16_pair(uint16_t c1, uint16_t c2)
{
	uint32_t code;

	Assert(0xD800 <= c1 && c1 <= 0xDBFF);
	Assert(0xDC00 <= c2 && c2 <= 0xDFFF);

	code = 0x10000;
	code += (c1 & 0x03FF) << 10;
	code += (c2 & 0x03FF);

	return code;
}

text decode(text *in_text, const char *unreserved_special)
{
	text	   result;
	char		*read_ptr = VARDATA(in_text);
	char		*write_ptr;
	int		len =VARSIZE(in_text) - VARHDRSZ;
	int		real_len;
	int		processed;

	real_len = (int) in_text->size();
	result.resize(real_len);

	write_ptr = (char*) result.c_str();

	processed = 0;
	while (processed < len)
	{
		if (*read_ptr != '%')
		{
			char	c = *read_ptr;

			if ((c >='0' && c <= '9') ||
			    (c >= 'a' && c <= 'z') ||
			    (c >= 'A' && c <= 'Z') ||
			    (strchr(unreserved_special, c) != NULL))
			{
				*write_ptr++ = c;
				real_len += 1;
				processed += 1;
				read_ptr += 1;
			}
			else
				elog(ERROR, "unaccepted chars in url code");
		}
		else
		{
			if (processed + 1 >= len)
				elog(ERROR, "incomplete input string");

			/* next two/four chars are part of UTF16 char */
			if (read_ptr[1] == 'u' || read_ptr[1] == 'U')
			{
				unsigned char	b1;
				unsigned char	b2;
				uint32_t	u;
				unsigned char	buffer[10];
				buffer[0] = 0;
				int		utf8len;

				uint16_t c1;
				uint16_t c2;

				/* read first two bytes */
				if (processed + 6 > len)
					elog(ERROR, "incomplete input string");

				b1 = (get_hex(read_ptr[2]) << 4) | get_hex(read_ptr[3]);
				b2 = (get_hex(read_ptr[4]) << 4) | get_hex(read_ptr[5]);

				/*
				 * expect input in UTF16-BE (Big Endian) and convert it
				 * to LE used by Intel.
				 */
				c1 = b2 | (b1 << 8);

				/* is surrogate pairs */
				if (0xD800 <= c1 && c1 <= 0xDBFF)
				{
					if (processed + 10 > len)
						elog(ERROR, "incomplete input string");

					b1 = (get_hex(read_ptr[6]) << 4) | get_hex(read_ptr[7]);
					b2 = (get_hex(read_ptr[8]) << 4) | get_hex(read_ptr[9]);
					c2 = b2 | (b1 << 8);

					if (!(0xDC00 <= c2 && c2 <= 0xDFFF))
						elog(ERROR, "invalid utf16 input char");

					u = decode_utf16_pair(c1, c2);
					processed += 10;
					read_ptr += 10;
				}
				else
				{
					u = c1;
					processed += 6;
					read_ptr += 6;
				}

				unicode_to_utf8((pg_wchar) u, buffer);
				utf8len = pg_utf_mblen(buffer);
#pragma warning(suppress : 4996)
				strncpy(write_ptr, (const char *) buffer, utf8len);
				write_ptr += utf8len;
				real_len += utf8len;
			}
			else
			{
				/*
				 * next two/three chars are part of UTF8 char, but it can
				 * be decoded byte by byte.
				 */
				if (processed + 3 > len)
					elog(ERROR, "incomplete input string");

				*((unsigned char *) write_ptr++) = (get_hex(read_ptr[1]) << 4) | get_hex(read_ptr[2]);
				real_len += 1;
				processed += 3;
				read_ptr += 3;
			}
		}
	}

//	SET_VARSIZE(result, real_len + VARHDRSZ);

	return result;
}


/*
 * encode input string to url encode
 *
 */
Datum
url_encode(PG_FUNCTION_ARGS)
{
	PG_RETURN_TEXT_P(encode(PG_GETARG_TEXT_P(0), ".-~_"));
}

/*
 * decode input string from url encode
 *
 */
Datum
url_decode(PG_FUNCTION_ARGS)
{
	PG_RETURN_TEXT_P(decode(PG_GETARG_TEXT_P(0), ".-~_"));
}

Datum
uri_encode(PG_FUNCTION_ARGS)
{
	PG_RETURN_TEXT_P(encode(PG_GETARG_TEXT_P(0), "-_.!~*'();/?:@&=+$,#"));
}

Datum
uri_decode(PG_FUNCTION_ARGS)
{
	PG_RETURN_TEXT_P(decode(PG_GETARG_TEXT_P(0), "-_.!~*'();/?:@&=+$,#"));
}

/*
CREATE EXTENSION url_encode;
CREATE EXTENSION

postgres=# SELECT url_encode('Ahoj Světe'), url_encode('Hello World');
    url_encode     │  url_encode   
───────────────────┼───────────────
 Ahoj%20Sv%C4%9Bte │ Hello%20World
(1 row)

 postgres=# SELECT url_decode('Ahoj%20Sv%C4%9Bte'), url_decode('Hello%20World');
 url_decode │ url_decode  
────────────┼─────────────
 Ahoj Světe │ Hello World
(1 row)

postgres=# select uri_encode('http://hu.wikipedia.org/wiki/São_Paulo');
                 uri_encode                  
---------------------------------------------
 http://hu.wikipedia.org/wiki/S%C3%A3o_Paulo

postgres=# select uri_decode('http://hu.wikipedia.org/wiki/S%C3%A3o_Paulo');
               uri_decode               
----------------------------------------
 http://hu.wikipedia.org/wiki/São_Paulo
 */