// url_path2.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#if 0
classes: POSIX_path WIN32_path UNC_path URL_path URI_path Splitter
Utf_string Wide_string

file:///Testing/exr/cathedral/cathedral_withdisparity_%25V.%23%23%23%23.exr <-> /Testing/exr/cathedral/cathedral_withdisparity_%V.####.exr
file:///i%20/am/an%20awk%C3%A4d/%F0%9F%8E%83%20uni%C4%86%C3%B8%E2%88%82e/path <-> /i /am/an awk�d/?? uni?�?e/path

For the UNC Windows file path
\\laptop\My Documents\FileSchemeURIs.doc

The corresponding valid file URI in Windows is the following:
file://laptop/My%20Documents/FileSchemeURIs.doc

For the local Windows file path
C:\Documents and Settings\davris\FileSchemeURIs.doc

The corresponding valid file URI in Windows is:
file:///C:/Documents%20and%20Settings/davris/FileSchemeURIs.doc

The important factors here are the use of percent-encoding and the number of slashes following the �file:� scheme name.

https://learn.microsoft.com/en-us/archive/blogs/ie/file-uris-in-windows

//--

UNC Windows path: \\sharepoint.business.com\DavWWWRoot\rs\project 1\document.txt

becomes

URI: file://sharepoint.business.com/DavWWWRoot/rs/project%201/document.txt

Using PowerShell

Converting the above UNC path into a file URI is extremely simple using PowerShell (all versions), and requires only the format and replace operators, for example:

$Path = "\\sharepoint.business.com\DavWWWRoot\rs\project 1\document.txt"

# replace back slash characters with a forward slash, url-encode spaces,
# and then prepend "file:" to the resulting string

# note: the "\\" in the first use of the replace operator is an escaped
# (single) back slash, and resembles the leading "\\" in the UNC path
# by coincidence only

"file:{0}" -f ($Path -replace "\\", "/" -replace " ", "%20")

Which yields the following:

file://sharepoint.business.com/DavWWWRoot/rs/project%201/document.txt

//---

here's the function I use to convert a normal path to an UNC path:

wstring ConvertToUNC(wstring sPath)
{
    WCHAR temp;
    UNIVERSAL_NAME_INFO * puni = NULL;
    DWORD bufsize = 0;
    wstring sRet = sPath;
    //Call WNetGetUniversalName using UNIVERSAL_NAME_INFO_LEVEL option
    if (WNetGetUniversalName(sPath.c_str(),
        UNIVERSAL_NAME_INFO_LEVEL,
        (LPVOID) &temp,
        &bufsize) == ERROR_MORE_DATA)
    {
        // now we have the size required to hold the UNC path
        WCHAR * buf = new WCHAR[bufsize+1];
        puni = (UNIVERSAL_NAME_INFO *)buf;
        if (WNetGetUniversalName(sPath.c_str(),
            UNIVERSAL_NAME_INFO_LEVEL,
            (LPVOID) puni,
            &bufsize) == NO_ERROR)
        {
            sRet = wstring(puni->lpUniversalName);
        }
        delete [] buf;
    }

    return sRet;;
} 

//--
std::string normalizePath(const std::string& messyPath) {
    return messyPath.lexically_normal();
}

The C++11 solution is to wrap the UTF-8 stream in an appropriate wbuffer_convert

#include <fstream>
#include <string>
#include <codecvt>
int main()
{
    std::ifstream utf8file("test.txt"); // if the file holds UTF-8 data
    std::wbuffer_convert<std::codecvt_utf8<wchar_t>> conv(utf8file.rdbuf());
    std::wistream ucsbuf(&conv);
    std::wstring line;
    getline(ucsbuf, line); // then line holds UCS2 or UCS4, depending on the OS
}



#include <cctype>
#include <iomanip>
#include <sstream>
#include <string>

using namespace std;

string url_encode(const string &value) {
    ostringstream escaped;
    escaped.fill('0');
    escaped << hex;

    for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
        string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << uppercase;
        escaped << '%' << setw(2) << int((unsigned char) c);
        escaped << nouppercase;
    }

    return escaped.str();
}

include <Crtdbg.h>    // for debug stuff
#include "Winnetwk.h"  // for WNetGetUniversalName()
#include "Lm.h"        // for NetShareGetInfo()
#include "pystring.h"  // from http://code.google.com/p/pystring

#pragma comment( lib, "Mpr.lib" )       // for WNetGetUniversalName()
#pragma comment( lib, "Netapi32.lib" )  // for NetShareGetInfo()

//-----------------------------------------------------------------------------
// converts x:\\folder -> \\\\server\\share\\folder
bool ConvertLocalPathToUNC(const char* szFilePath, std::string& strUNC)
{
  // get size of the remote name buffer
  DWORD dwBufferSize = 0;
  char szBuff[2];
  if (::WNetGetUniversalName(szFilePath, UNIVERSAL_NAME_INFO_LEVEL, szBuff, &dwBufferSize) == ERROR_MORE_DATA)
  {
    // get remote name of the share
    char* buf = new char[dwBufferSize];
    UNIVERSAL_NAME_INFO* puni = (UNIVERSAL_NAME_INFO*) buf;

    if (::WNetGetUniversalName(szFilePath, UNIVERSAL_NAME_INFO_LEVEL, buf, &dwBufferSize) == NO_ERROR)
    {
      strUNC = puni->lpUniversalName;
      delete [] buf;
      return true;
    }
    delete [] buf;
  }

  return false;
}

//-----------------------------------------------------------------------------
// converts \\\\server\\share\\folder -> x:\\folder
bool ConvertUNCToLocalPath(const char* szUNC, std::string& strLocalPath)
{
  // get share name from UNC
  std::string strUNC(szUNC);
  std::vector< std::string > vecTokens;
  pystring::split(strUNC, vecTokens, _T("\\"));

  if (vecTokens.size() < 4)
    return false;

  // we need wchar for NetShareGetInfo()
  std::wstring strShare(vecTokens[3].length(), L' ');
  std::copy(vecTokens[3].begin(), vecTokens[3].end(), strShare.begin());

  PSHARE_INFO_502  BufPtr;
  NET_API_STATUS   res;
  if ((res = NetShareGetInfo(NULL, const_cast<LPWSTR>(strShare.c_str()), 502, (LPBYTE*) &BufPtr)) == ERROR_SUCCESS)
  {
    // print the retrieved data.
    _RPTF3(_CRT_WARN, _T("%ls\t%ls\t%u\n"), BufPtr->shi502_netname, BufPtr->shi502_path, BufPtr->shi502_current_uses);

    std::wstring strPath(BufPtr->shi502_path);
    strLocalPath.assign(strPath.begin(), strPath.end());

    // build local path
    for (size_t i = 4; i < vecTokens.size(); ++i)
    {
      if (!pystring::endswith(strLocalPath, _T("\\")))
        strLocalPath += _T("\\");
      strLocalPath += vecTokens[i];
    }

    // Validate the value of the shi502_security_descriptor member.
    if (IsValidSecurityDescriptor(BufPtr->shi502_security_descriptor))
      _RPTF0(_CRT_WARN, _T("It has a valid Security Descriptor.\n"));
    else
      _RPTF0(_CRT_WARN, _T("It does not have a valid Security Descriptor.\n"));

    // Free the allocated memory.
    NetApiBufferFree(BufPtr);
  }
  else
    return false;

  return true;
}

x = �red,orange,yellow� 
x.split(�,�) 
[�red�, �orange�, �yellow�] 

An SSH connection to the host host.example.com on the standard port using username user.

     ssh://user@host.example.com

An SSH connection to the host host.example.com on port 2222 using username user.

     ssh://user@host.example.com:2222

An SSH connection to the host having the specified host-key fingerprint at host.example.com on the standard port using username user.

     ssh://user;fingerprint=ssh-dss-c1-b1-30-29-d7-b8-de-6c-97-
          77-10-d7-46-41-63-87@host.example.com
#endif

#include <iostream>
using namespace std;

void Usage()
{	cout << "Usage: url_path2 " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "url_path2 starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}

	cout << "url_path2 done!" << endl;
	return ok;
}
