// UrlPath.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef UrlPath_h
#define UrlPath_h

#include <iostream>
#include <string>

/* 

The URI generic syntax consists of five components organized hierarchically in order of decreasing significance from left to right:[26]

URI = scheme ":" ["//" authority] path ["?" query] ["#" fragment]

A component is undefined if it has an associated delimiter and the delimiter does not appear in the URI; the scheme and path components are always defined.[27] A component is empty if it has no characters; the scheme component is always non-empty.[26]

The authority component consists of subcomponents:

authority = [userinfo "@"] host [":" port]
*/

struct Authority
{	std::string userinfo;
	std::string host;
	std::string port;
};

class UrlPath
{	UrlPath(const UrlPath&) = delete;
	void operator=(const UrlPath&) = delete;
	bool is_good;
	bool Parse(const char* s);
public:
	std::string scheme;
	Authority authority;
	std::string path;
	std::string query;
	std::string fragment;
	~UrlPath()
	{}
	UrlPath()
	:	is_good(false)
	{}
	UrlPath(const char* s)
	{	is_good = Parse(s);
	}
	bool operator!() const
	{	return !is_good;
	}
	bool operator!=(const std::string& s) const
	{	return 0 != strcmp(path.c_str(),s.c_str());
	}
	std::string Encode();
	std::string Decode();
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const UrlPath& urlPath)
{	return urlPath.Print(os);
}


inline
std::istream& operator>>(std::istream& is,UrlPath& urlPath)
{	return urlPath.Input(is);
}

#endif
