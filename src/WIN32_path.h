// WIN32_path.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef WIN32_path_h
#define WIN32_path_h

#include <iostream>

class WIN32_path
{	WIN32_path(const WIN32_path&) = delete;
	void operator=(const WIN32_path&) = delete;

public:
	~WIN32_path()
	{}
	WIN32_path()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const WIN32_path& wIN32_path)
{	return wIN32_path.Print(os);
}


inline
std::istream& operator>>(std::istream& is,WIN32_path& wIN32_path)
{	return wIN32_path.Input(is);
}

#endif
