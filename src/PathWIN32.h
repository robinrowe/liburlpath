// PathWIN32.h 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#ifndef PathWIN32_h
#define PathWIN32_h

#include <iostream>

class PathWIN32
{	PathWIN32(const PathWIN32&) = delete;
	void operator=(const PathWIN32&) = delete;

public:
	~PathWIN32()
	{}
	PathWIN32()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const PathWIN32& pathWIN32)
{	return pathWIN32.Print(os);
}


inline
std::istream& operator>>(std::istream& is,PathWIN32& pathWIN32)
{	return pathWIN32.Input(is);
}

#endif
