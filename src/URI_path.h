// URI_path.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef URI_path_h
#define URI_path_h

#include <iostream>

class URI_path
{	URI_path(const URI_path&) = delete;
	void operator=(const URI_path&) = delete;

public:
	~URI_path()
	{}
	URI_path()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const URI_path& uRI_path)
{	return uRI_path.Print(os);
}


inline
std::istream& operator>>(std::istream& is,URI_path& uRI_path)
{	return uRI_path.Input(is);
}

#endif
