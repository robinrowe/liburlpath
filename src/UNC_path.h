// UNC_path.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef UNC_path_h
#define UNC_path_h

#include <iostream>

class UNC_path
{	UNC_path(const UNC_path&) = delete;
	void operator=(const UNC_path&) = delete;

public:
	~UNC_path()
	{}
	UNC_path()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const UNC_path& uNC_path)
{	return uNC_path.Print(os);
}


inline
std::istream& operator>>(std::istream& is,UNC_path& uNC_path)
{	return uNC_path.Input(is);
}

#endif
