// url_encoder.h
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef url_encoder_h
#define url_encoder_h

#include <string>
#include <stdint.h>

typedef std::string text;

text encode(text* in_text, const char* unreserved_special);
text decode(text* in_text, const char* unreserved_special);

#endif