// POSIX_path.cpp
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include "POSIX_path.h"
using namespace std;

ostream& POSIX_path::Print(ostream& os) const
{	// to-do
	return os << "POSIX_path";
} 

istream& POSIX_path::Input(std::istream& is) 
{	// to-do
	return is;
}
