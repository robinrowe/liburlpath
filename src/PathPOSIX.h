// PathPOSIX.h 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#ifndef PathPOSIX_h
#define PathPOSIX_h

#include <iostream>

class PathPOSIX
{	PathPOSIX(const PathPOSIX&) = delete;
	void operator=(const PathPOSIX&) = delete;

public:
	~PathPOSIX()
	{}
	PathPOSIX()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const PathPOSIX& pathPOSIX)
{	return pathPOSIX.Print(os);
}


inline
std::istream& operator>>(std::istream& is,PathPOSIX& pathPOSIX)
{	return pathPOSIX.Input(is);
}

#endif
