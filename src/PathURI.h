// PathURI.h 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#ifndef PathURI_h
#define PathURI_h

#include <iostream>

class PathURI
{	PathURI(const PathURI&) = delete;
	void operator=(const PathURI&) = delete;

public:
	~PathURI()
	{}
	PathURI()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const PathURI& pathURI)
{	return pathURI.Print(os);
}


inline
std::istream& operator>>(std::istream& is,PathURI& pathURI)
{	return pathURI.Input(is);
}

#endif
