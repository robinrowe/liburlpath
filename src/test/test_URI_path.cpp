// test_URI_path.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include "../URI_path.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing URI_path" << endl;
	URI_path uRI_path;
	if(!uRI_path)
	{	cout << "URI_path failed on operator!" << endl;
		return 1;
	}
	cout << uRI_path << endl;
	cout << "URI_path Passed!" << endl;
	return 0;
}
