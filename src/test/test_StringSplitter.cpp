// test_StringSplitter.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include "../StringSplitter.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing StringSplitter" << endl;
	StringSplitter stringSplitter;
	if(!stringSplitter)
	{	cout << "StringSplitter failed on operator!" << endl;
		return 1;
	}
	cout << stringSplitter << endl;
	cout << "StringSplitter Passed!" << endl;
	return 0;
}
