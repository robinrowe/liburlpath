// test_WIN32_path.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include "../WIN32_path.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing WIN32_path" << endl;
	WIN32_path wIN32_path;
	if(!wIN32_path)
	{	cout << "WIN32_path failed on operator!" << endl;
		return 1;
	}
	cout << wIN32_path << endl;
	cout << "WIN32_path Passed!" << endl;
	return 0;
}
