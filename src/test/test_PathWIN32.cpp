// test_PathWIN32.cpp 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#include <iostream>
#include "../PathWIN32.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing PathWIN32" << endl;
	PathWIN32 pathWIN32;
	if(!pathWIN32)
	{	cout << "PathWIN32 failed on operator!" << endl;
		return 1;
	}
	cout << pathWIN32 << endl;
	cout << "PathWIN32 Passed!" << endl;
	return 0;
}
