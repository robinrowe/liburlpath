// test_POSIX_path.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include "../POSIX_path.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing POSIX_path" << endl;
	POSIX_path pOSIX_path;
	if(!pOSIX_path)
	{	cout << "POSIX_path failed on operator!" << endl;
		return 1;
	}
	cout << pOSIX_path << endl;
	cout << "POSIX_path Passed!" << endl;
	return 0;
}
