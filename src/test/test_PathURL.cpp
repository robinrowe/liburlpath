// test_PathURL.cpp 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#include <iostream>
#include "../PathURL.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing PathURL" << endl;
	PathURL pathURL;
	if(!pathURL)
	{	cout << "PathURL failed on operator!" << endl;
		return 1;
	}
	cout << pathURL << endl;
	cout << "PathURL Passed!" << endl;
	return 0;
}
