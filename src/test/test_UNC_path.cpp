// test_UNC_path.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include "../UNC_path.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing UNC_path" << endl;
	UNC_path uNC_path;
	if(!uNC_path)
	{	cout << "UNC_path failed on operator!" << endl;
		return 1;
	}
	cout << uNC_path << endl;
	cout << "UNC_path Passed!" << endl;
	return 0;
}
