// test_URL_path.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include "../URL_path.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing URL_path" << endl;
	URL_path uRL_path;
	if(!uRL_path)
	{	cout << "URL_path failed on operator!" << endl;
		return 1;
	}
	cout << uRL_path << endl;
	cout << "URL_path Passed!" << endl;
	return 0;
}
