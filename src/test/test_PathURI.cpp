// test_PathURI.cpp 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#include <iostream>
#include "../PathURI.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing PathURI" << endl;
	PathURI pathURI;
	if(!pathURI)
	{	cout << "PathURI failed on operator!" << endl;
		return 1;
	}
	cout << pathURI << endl;
	cout << "PathURI Passed!" << endl;
	return 0;
}
