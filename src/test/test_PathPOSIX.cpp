// test_PathPOSIX.cpp 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#include <iostream>
#include "../PathPOSIX.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing PathPOSIX" << endl;
	PathPOSIX pathPOSIX;
	if(!pathPOSIX)
	{	cout << "PathPOSIX failed on operator!" << endl;
		return 1;
	}
	cout << pathPOSIX << endl;
	cout << "PathPOSIX Passed!" << endl;
	return 0;
}
