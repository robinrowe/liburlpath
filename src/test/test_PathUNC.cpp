// test_PathUNC.cpp 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#include <iostream>
#include "../PathUNC.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing PathUNC" << endl;
	PathUNC pathUNC;
	if(!pathUNC)
	{	cout << "PathUNC failed on operator!" << endl;
		return 1;
	}
	cout << pathUNC << endl;
	cout << "PathUNC Passed!" << endl;
	return 0;
}
