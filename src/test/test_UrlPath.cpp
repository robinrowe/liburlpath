// test_UrlPath.cpp 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include <iostream>
#include <string>
#include <string.h>
#include "../UrlPath.h"
using namespace std;

struct Transform
{	const char* plain_text;
	const char* url_text;
};

static Transform t[] =
{	"hello","hello",
	"Ahoj Světe","Ahoj%20Sv%C4%9Bte",
	"Hello World","Hello%20World",
	0,0
};

int main(int argc,char* argv[])
{	cout << "Testing UrlPath" << endl;
	unsigned i = 0;
	while(t->plain_text[i])
	{	UrlPath plain_text(t[i].plain_text);
		UrlPath url_text(t[i].url_text);
		if(!plain_text || !url_text)
		{	cout << "UrlPath failed on operator!" << endl;
			return 1;
		}
		cout << "\"" << plain_text << "\" -> \"" << url_text << "\" ";
		string decode = url_text.Decode();
		string encode = plain_text.Encode();
		if(plain_text != decode || url_text != encode)
		{	cout << "failed!" << endl;
			cout << "\"" << plain_text << "\" : \"" << url_text << "\"" << endl;
			return -1;
		}
		else
		{	cout << "good" << endl;
		}
		i++;
	}
	UrlPath urlPath("bye");
	cout << urlPath << endl;
	cout << "UrlPath Passed!" << endl;
	return 0;
}
