// UrlPath.cpp
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#include "UrlPath.h"
#include "url_encode.h"
using namespace std;

ostream& UrlPath::Print(ostream& os) const
{	return os << path;
} 

istream& UrlPath::Input(std::istream& is) 
{	// to-do
	return is;
}

bool UrlPath::Parse(const char* s)
{	path = s;
	// to-do
	return true;
}

string UrlPath::Encode()
{	text in_text(path);
	return encode(&in_text,".-~_");
}

string UrlPath::Decode()
{	text in_text(path);
	return decode(&in_text,".-~_");
}
