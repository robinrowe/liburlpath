// POSIX_path.h 
// Created by Robin Rowe 2023-09-27
// MIT Open Source

#ifndef POSIX_path_h
#define POSIX_path_h

#include <iostream>

class POSIX_path
{	POSIX_path(const POSIX_path&) = delete;
	void operator=(const POSIX_path&) = delete;

public:
	~POSIX_path()
	{}
	POSIX_path()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const POSIX_path& pOSIX_path)
{	return pOSIX_path.Print(os);
}


inline
std::istream& operator>>(std::istream& is,POSIX_path& pOSIX_path)
{	return pOSIX_path.Input(is);
}

#endif
