// PathUNC.h 
// Created by Robin Rowe 2023-09-28
// MIT Open Source

#ifndef PathUNC_h
#define PathUNC_h

#include <iostream>

class PathUNC
{	PathUNC(const PathUNC&) = delete;
	void operator=(const PathUNC&) = delete;

public:
	~PathUNC()
	{}
	PathUNC()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const PathUNC& pathUNC)
{	return pathUNC.Print(os);
}


inline
std::istream& operator>>(std::istream& is,PathUNC& pathUNC)
{	return pathUNC.Input(is);
}

#endif
