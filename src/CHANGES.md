#CHANGES.md

Robin Rowe 2023-09-27

cmaker: project URLpath
cmaker: class UrlPath
cmaker: program urlpath
cmaker: program url_path
cmaker: program url_path2
cmaker: class POSIX_path
cmaker: class WIN32_path
cmaker: class UNC_path
cmaker: class URL_path
cmaker: class URI_path
cmaker: class StringSplitter
cmaker: class PathPOSIX
cmaker: class PathWIN32
cmaker: class PathUNC
cmaker: class PathURL
cmaker: class PathURI
