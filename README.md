# liburipath

This library has C++ classes and a C API for converting between different types of file paths.

Created September 27th, 2023. Nothing usable yet, check back after October 27th, 2023.

## OAIO Notes

https://github.com/foundrytom/OpenAssetIO/

src/openassetio-core/src/utils/path.cpp

Not: https://github.com/OpenAssetIO/OpenAssetIO

Pybind C++ and no C API.

Imaging something like this:

     path = "C:\hello\world"
     MAX_STR = 3 * len(path)
     path2 = create_string_buffer(MAX_STR)
     ok = dll.path_win32_posix(path,path2,MAX_STR)

Use pybind, implement the C++ methods here:

 https://github.com/foundrytom/OpenAssetIO/commit/da0f24fa7c2490796685467217a7c086a20ff815#diff-1033366551aac98c0e357d3a195653e8ba027f714df4774270c86b5166eb0f2dR11

src/openassetio-core/src/utils/path.cpp

enum class PathType { kSystem = 0, kPOSIX, kWindows };

Str pathToFileURL(StrView absolutePath, PathType pathType) {
  throw errors::NotImplementedException("pathToFileURL not yet implemented");
}

Str pathFromFileURL(StrView fileURL, PathType pathType) {
  throw errors::NotImplementedException("pathFromFileURL not yet implemented");
}

* https://github.com/OpenAssetIO/OpenAssetIO/issues/1117#issuecomment-1739268426
* https://github.com/ada-url/ada
* https://learn.microsoft.com/en-us/dotnet/standard/io/file-path-formats#identify-the-path


# Classes
    
- PathPOSIX: e.g., /DavWWWRoot/rs/project 1/document.txt 
- PathWIN32: e.g., C:\\DavWWWRoot\\rs\\project 1\\document.txt
- PathUNC: e.g., \\sharepoint.business.com\DavWWWRoot\rs\project 1\document.txt
- PathURL: e.g., file://sharepoint.business.com/DavWWWRoot/rs/project%201/document.txt
- PathURI: any of the above
- StringSplitter:
```Cpp
	StringSpliter s("red,orange,yellow");
	s.split(",");
	// s[0] = red, s[1] = orange, s[2] = yellow
```
## Programs

	path_posix <path>
	path_win32 <path>
	path_unc <path>
	path_url <path>

Detects path type in path given and converts to type specified.

## C API

	char* path_posix_win32(const char* path);
	char* path_posix_unc(const char* path);
	char* path_posix_url(const char* path);
	char* path_win32_posix(const char* path);
	char* path_win32_unc(const char* path);
	char* path_win32_url(const char* path);
	char* path_unc_posix(const char* path);
	char* path_unc_win32(const char* path);
	char* path_unc_url(const char* path);
	char* path_url_posix(const char* path);
	char* path_url_win32(const char* path);
	char* path_url_unc(const char* path);
	void path_free(char* p);

	int path_posix_win32(const char* path,char* path2,int length);
	int path_posix_unc(const char* path,char* path2,int length);
	int path_posix_url(const char* path,char* path2,int length);
	int path_win32_posix(const char* path,char* path2,int length);
	int path_win32_unc(const char* path,char* path2,int length);
	int path_win32_url(const char* path,char* path2,int length);
	int path_unc_posix(const char* path,char* path2,int length);
	int path_unc_win32(const char* path,char* path2,int length);
	int path_unc_url(const char* path,char* path2,int length);
	int path_url_posix(const char* path,char* path2,int length);
	int path_url_win32(const char* path,char* path2,int length);
	int path_url_unc(const char* path,char* path2,int length);


## Unit Tests

	ctest

## Roadmap

* Due for integration and release with OpenAssetsIO on October 27th, 2023
* May be proposed eventually to the ISO C++ std::filesystem standard

## Author

Robin Rowe <robin.rowe@cinepaint.org>

## License

MIT Open Source.

